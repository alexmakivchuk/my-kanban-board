import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';

import {AuthModule} from './authorization/auth.module';
import {AppRoutingModule} from './app-routing.module';
import {SharedModule} from './shared/shared.module';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {UsersService} from './shared/services/users.service';
import {AuthService} from './shared/services/auth.service';
import {TasksService} from './shared/services/tasks.service';
import { Page404Component } from './page404/page404.component';





@NgModule({
  declarations: [
    AppComponent,
    Page404Component,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AuthModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
  ],
  providers: [
    UsersService,
    AuthService,
    TasksService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {

}

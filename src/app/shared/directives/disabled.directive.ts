import { Directive } from '@angular/core';

@Directive({
  selector: '[appDisabled]'
})
export class DisabledDirective {
  public inputValue: false;
  constructor() { }

}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Task} from '../task/task.model';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  constructor(private http: HttpClient) {

  }
   getTasks(id: number) {
    return this.http.get(`http://localhost:3000/posts?id=${id}`)
      .pipe(map((response: Response) => response) ).
       pipe(map((user) => user[0] ? user[0] : undefined));

  }
  addNewTask(task: Task, id: number) {
    return this.http.put('http://localhost:3000/posts/' + id, task)
      .pipe(map((response: Response) => response) );
  }

}

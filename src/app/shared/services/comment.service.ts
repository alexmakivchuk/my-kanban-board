import { Injectable } from '@angular/core';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {TaskComment} from '../comment/task-comment.model';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  addNewComment(comment: TaskComment) {
    return this.http.post('http://localhost:3000/comments', comment)
      .pipe(map((response: Response) => response) );
  }
  saveComment(comment: TaskComment) {
    return this.http.put('http://localhost:3000/comments/' + comment.id, comment)
      .pipe(map((response: Response) => response) );
  }
  getTaskComment(name: string) {
    return this.http.get(`http://localhost:3000/comments?name=${name}`)
      .pipe(map((response: Response) => response) ).
      pipe(map((user) => user[0] ? user[0] : undefined));

  }
}

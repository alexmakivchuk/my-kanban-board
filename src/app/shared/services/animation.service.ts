import {EventEmitter, Injectable, Output} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AnimationService {
  private clickCnt: string;
  onClick: EventEmitter<string> = new EventEmitter();
  sysOnClick: EventEmitter<string> = new EventEmitter();
  public doClick(data: string) {
    this.onClick.emit(data);
  }
  public sysDoClick(data: string) {
    this.sysOnClick.emit(data);
  }
  constructor(
     ) { }


}


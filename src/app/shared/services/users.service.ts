import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../user/user.model';
// tslint:disable-next-line:no-unused-expression
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) {

  }
  getUserByEmail(email: string) {
    return this.http.get(`http://localhost:3000/users?email=${email}`)
      .pipe(map((response: Response) => response))
      .pipe(map((user) => user[0] ? user[0] : undefined));

  }
  createNewUser(user: User) {
    return this.http.post('http://localhost:3000/users', user)
      .pipe(map((response: Response) => response) );

  }
  saveUserData(user: User) {
    return this.http.put('http://localhost:3000/users/' + user.id, user)
      .pipe(map((response: Response) => response) );
  }



}

import {animate, state, style, transition, trigger} from '@angular/animations';

export const fade1StateTrigger = trigger('fade1', [
  transition(':enter', [
    style({ opacity: 0 }),
    animate(500, style({ opacity: 1 })),
  ])/*,
  transition(':leave', [
    animate(500, style({ opacity: 0 }))
  ])*/
]);



import {animate, state, style, transition, trigger} from '@angular/animations';

export const fadeStateTrigger = trigger('fade', [
  state('open', style({
    opacity: 1,
  })),
  state('closed', style({
    opacity: 0,
  })),
  transition('open -> closed', [animate(500 )]),
  transition('closed -> open', [animate(500 )])
]);


  /*trigger('fadeDiv', [
    state('start', style({
      opacity: 0,
    })),
    state('end', style({
      opacity: 0,
    })),
    transition('start -> end', animate(1500)),
    transition('end -> start', animate(1500))
  ]);*/

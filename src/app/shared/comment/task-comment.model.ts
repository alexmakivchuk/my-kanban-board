export class TaskComment {
  constructor(
    public name: string,
    public comment: string,
    public id?: number
) {}
}

import {Component, HostBinding, OnInit} from '@angular/core';
import {fadeStateTrigger} from '../shared/animations/fade.animations';
import {Router} from '@angular/router';
import {AnimationService} from '../shared/services/animation.service';

@Component({
  selector: 'app-system',
  templateUrl: './system.component.html',
  styleUrls: ['./system.component.scss'],
  animations: [fadeStateTrigger]
})
export class SystemComponent implements OnInit {
  public clickCnt = 'closed';
  constructor(
    private animationService: AnimationService
  ) {
    this.animationService.sysOnClick.subscribe(cnt => this.clickCnt = cnt);
  }
  ngOnInit() {
    setTimeout(() => {
      this.clickCnt = 'open';
    }, 100);
  }
}

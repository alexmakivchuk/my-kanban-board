import {Component, HostBinding, HostListener, OnDestroy, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {Router} from '@angular/router';
import {UsersService} from '../../../shared/services/users.service';
import {AuthService} from '../../../shared/services/auth.service';
import {fadeStateTrigger} from '../../../shared/animations/fade.animations';
import {AnimationService} from '../../../shared/services/animation.service';
import {TasksService} from '../../../shared/services/tasks.service';
import {CommentService} from '../../../shared/services/comment.service';

@Component({
  selector: 'app-planing-page',
  templateUrl: './planing-page.component.html',
  styleUrls: ['./planing-page.component.scss'],
  animations: [ fadeStateTrigger ]
})

export class PlaningPageComponent implements OnInit, OnDestroy {

  public modalTask: string;
  public commentId: number;
  public taskStatus: string;
  clickCnt: string;
  taskComment: string;
   comment = {
     name: '',
     comment: '',
     id: null };

  open = 'open';
  closed = 'closed';
  a = this.closed;
  form: FormGroup;
  public visibleSidebar;
  public name = '';
  public user = JSON.parse(localStorage.getItem('user'));
  public task = JSON.parse(localStorage.getItem('task'));
  public id = this.user.id;
  reveiw = this.task.reveiw;
  production = this.task.production;
  todo = this.task.todo;

  inProgress = this.task.inProgress;

  complete = this.task.complete;
  constructor(
    private router: Router,
    private animationService: AnimationService,
    private usersService: UsersService,
    private authService: AuthService,
    private tasksService: TasksService,
    private commentService: CommentService,
  ) {
    this.animationService.sysOnClick.subscribe(cnt => this.clickCnt = cnt);
    this.animationService.onClick.subscribe(cnt => this.clickCnt = cnt);
  }
  ngOnInit() {
    setTimeout(() => {
      this.a = this.open;
    }, 100);
    this.user.password = '';
    window.localStorage.setItem('user', JSON.stringify(this.user));
  }


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    this.addNewTask();
  }

  addToDo() {
    if (this.name !== '') {
      this.todo.push( this.name);
      this.comment.name = this.name;
      this.comment.comment = '';
      this.name = '';
      this.addNewTask();
      console.log(this.comment);
      return new Promise(() => {
        this.commentService.addNewComment(this.comment).subscribe();
      });
    }
  }

  addNewTask() {
    window.localStorage.setItem('task', JSON.stringify(this.task));
    return new Promise(() => {
      this.tasksService.addNewTask(this.task, this.id).subscribe();
    });
  }

  removeElement(item: string) {
    for (let i = 0; i < this.todo.length; i++) {
      if (item === this.todo[i]) {
        this.todo.splice(i, 1);
        this.addNewTask();

      }
    }
  }

  removeElem(item: string) {
    for (let i = 0; i < this.complete.length; i++) {
      if (item === this.complete[i]) {
        this.complete.splice(i, 1);
        this.addNewTask();

      }
    }
  }

  toggle() {
    this.animationService.sysDoClick(this.closed);
    setTimeout(() => {
      this.router.navigate(['system/edit-user']);
      this.animationService.sysDoClick(this.open);
    }, 500);
  }

  Exit() {
    this.authService.logout();
    this.animationService.sysDoClick(this.closed);
    setTimeout(() => {
      this.router.navigate(['']);

      this.animationService.doClick(this.open);
    }, 500);
  }


  openModal(data, items) {
    this.commentService.getTaskComment(data).subscribe((comment) => {
      // this.comment = comment;
      window.localStorage.setItem('comment', JSON.stringify(comment));

      this.taskComment = comment.comment;
     });
    setTimeout(() => {
      this.visibleSidebar = true;
      this.modalTask = data;

      if (items === this.todo) {
        this.taskStatus = 'todo';
      } else if (items === this.inProgress) {
        this.taskStatus = 'inprogress';
      } else  if (items === this.complete) {
        this.taskStatus = 'complete';
      } else if (items === this.reveiw) {
        this.taskStatus = 'reveiw';
      } else {
        this.taskStatus = 'production';
      }
    }, 150);

  }

  @HostListener('window:beforeunload')
  ngOnDestroy() {
    if (this.authService.isAuthenticated === true) {
      this.addNewTask();
      window.localStorage.setItem('task', JSON.stringify(this.task));
    }
  }
}

import {Component, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../../../shared/user/user.model';
import {fadeStateTrigger} from '../../../shared/animations/fade.animations';
import {AnimationService} from '../../../shared/services/animation.service';
import {UsersService} from '../../../shared/services/users.service';
import {AuthService} from '../../../shared/services/auth.service';
import {Message} from '../../../shared/message/message.model';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
  animations: [fadeStateTrigger]
})
export class EditUserComponent implements OnInit, OnDestroy {
  a = 'closed';
  b = 'closed';
  public user = JSON.parse(localStorage.getItem('user'));
  message: Message;
  public password = '';

  public name = this.user.name;
  public surname = this.user.surname;
  public clickCnt;
  public passState = false;
  private userPassword = '';
  private newPassword = '';
  form: FormGroup;
  formPass: FormGroup;
  closed = 'closed';
  open = 'open';


  saveName() {
    this.user.password = '';
    this.user.name = this.name;
    window.localStorage.setItem('user', JSON.stringify(this.user));
    this.user.password = this.password;
    return new Promise(() => {
      this.usersService.saveUserData(this.user).subscribe();
    });
  }

  saveSurname() {
    this.user.password = '';
    this.user.surname = this.surname;
    window.localStorage.setItem('user', JSON.stringify(this.user));
    this.user.password = this.password;
    return new Promise(() => {
      this.usersService.saveUserData(this.user).subscribe();
    });
  }


  constructor(
    private router: Router,
    private usersService: UsersService,
    private authService: AuthService,
    private animationService: AnimationService
  ) {
    this.passState = false;
    this.animationService.sysOnClick.subscribe( cnt => this.clickCnt = cnt);

  }

  ngOnInit() {


    this.usersService.getUserByEmail(this.user.email).subscribe(
      (user) => {
        this.userPassword = user.password;
      }
    );

    this.form = new FormGroup({
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)]),
    });

  }
  isPassValid() {

    if (this.userPassword === this.password) {
      this.passState = true;
      this.b = this.open;
    } else { this.b = this.closed; }
    return this.userPassword === this.password;

  }
  isNewPassValid() {
  return this.newPassword.length >= 6;

  }
  yourDataValid(data: string) {
   return data.length > 0;
  }
  onSubmit() {
      this.user.password = this.newPassword;
      return new Promise(() => {
        this.usersService.saveUserData(this.user).subscribe();
      });
  }

  toggle() {
    this.animationService.sysDoClick(this.closed);
    setTimeout(() => {
      this.router.navigate(['system/planning']);
      this.animationService.sysDoClick(this.open);
    }, 500);
  }
  ngOnDestroy() {
  }

}

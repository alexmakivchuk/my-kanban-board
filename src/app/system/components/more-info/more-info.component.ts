import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {CommentService} from '../../../shared/services/comment.service';
import {TaskComment} from '../../../shared/comment/task-comment.model';

@Component({
  selector: 'app-more-info',
  templateUrl: './more-info.component.html',
  styleUrls: ['./more-info.component.scss']
})
export class MoreInfoComponent implements OnInit, OnChanges {
@Input() public name: string;
@Input() public status: string;
@Input() public taskComment: string;
public user = JSON.parse(localStorage.getItem('user'));
public comment: TaskComment;
 constructor(
   private commentService: CommentService
 ) {

  }

  ngOnChanges() {
    this.comment = JSON.parse(localStorage.getItem('comment'));
  }

  ngOnInit() {

  }

  saveChangesComment() {
   this.comment.comment = this.taskComment;
   window.localStorage.setItem('comment', JSON.stringify(this.comment));
    return new Promise(() => {
      this.commentService.saveComment(this.comment).subscribe();
    });

}
}

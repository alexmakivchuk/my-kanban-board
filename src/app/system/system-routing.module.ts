import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SystemComponent } from './system.component';
import {EditUserComponent} from './components/edit-user/edit-user.component';
import {PlaningPageComponent} from './components/planing-page/planing-page.component';


const routes: Routes = [
  {path: '', component: SystemComponent, children: [
    {path: 'edit-user', component: EditUserComponent},
    {path: 'planning', component: PlaningPageComponent}
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ],
})
export class SystemRoutingModule {

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {SystemRoutingModule} from './system-routing.module';
import { SystemComponent } from './system.component';
import {CdkDragDrop, DragDropModule, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material';
import { PlaningPageComponent } from './components/planing-page/planing-page.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import {SidebarModule} from 'primeng/sidebar';
import {ButtonModule} from 'primeng/button';
import { MoreInfoComponent } from './components/more-info/more-info.component';
import {InputTextareaModule} from 'primeng/inputtextarea';

@NgModule({
  declarations: [SystemComponent, PlaningPageComponent, EditUserComponent, MoreInfoComponent],
  imports: [
    FormsModule,
    CommonModule,
    SharedModule,
    SystemRoutingModule,
    DragDropModule,
    MatInputModule,
    SidebarModule,
    ButtonModule,
    InputTextareaModule
    ]
})
export class SystemModule { }

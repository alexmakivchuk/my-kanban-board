import {Component, HostBinding, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {fadeStateTrigger} from '../shared/animations/fade.animations';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {AnimationService} from '../shared/services/animation.service';
import {fade1StateTrigger} from '../shared/animations/fade1.animations';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  animations: [ fadeStateTrigger, fade1StateTrigger ]
})
export class AuthComponent implements OnInit {
  a = true;
  b = '';
  closed = 'closed';
  open = 'open';
  // c = '';
  clickCount: string;
  constructor(
    private router: Router,
    private animationService: AnimationService) {
    this.animationService.onClick.subscribe(cnt => this.clickCount = cnt);
    }

  ngOnInit() {
    this.router.navigate(['/login']);
    setTimeout(() => {
      this.clickCount = 'open';
    }, 100);
    // this.c = this.open;
  }

  // toggle() {
  //
  //   this.clickCount = this.closed;
  //   if (this.a === true) {
  //   }
  //   setTimeout(() => {
  //
  //     if (this.a === true) {
  //       this.a = !this.a;
  //       this.clickCount = this.open;
  //       this.router.navigate(['/registration']);
  //     } else {
  //       this.router.navigate(['/login']);
  //       this.a = !this.a;
  //       this.clickCount = this.open;
  //     }
  //   }, 500);
  // }
}

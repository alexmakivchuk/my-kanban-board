import {Component, HostBinding, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../../shared/services/users.service';
import {User} from '../../shared/user/user.model';
import {Router} from '@angular/router';
import {fadeStateTrigger} from '../../shared/animations/fade.animations';
import {AnimationService} from '../../shared/services/animation.service';
import {fade1StateTrigger} from '../../shared/animations/fade1.animations';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  animations: [fade1StateTrigger]
})
export class RegistrationComponent implements OnInit {
  public user: User;
  form: FormGroup;
  clickCnt: string;
  closed = 'closed';
  open = 'open';
  status = 'dev';

  constructor(
    private usersService: UsersService,
    private router: Router,
    private animationService: AnimationService,
  ) {
    this.animationService.onClick.subscribe( cnt => this.clickCnt = cnt);
  }
  ngOnInit() {
    this.form = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails.bind(this)),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)]),
      'name': new FormControl(null, [Validators.required]),
      'surname': new FormControl(null, [Validators.required]),
      'agree': new FormControl(false, [Validators.requiredTrue])
    });
  }

  onSubmit() {
    this.user = this.form.value;
    this.animationService.doClick(this.closed);
    this.user.status = this.status;

    setTimeout(() => {
      this.animationService.doClick(this.open);
      this.usersService.createNewUser(this.user).subscribe( () =>
        this.router.navigate(['/login'], {queryParams: {
            nowCanLoggin: true
          }
        })
      );
      },  510);


  }

  toggle() {
    this.animationService.doClick(this.closed);
    setTimeout(() => {
      this.router.navigate(['/login']);
      this.animationService.doClick(this.open);
    }, 500);
  }

  forbiddenEmails(control: FormControl): Promise <any> {
    return new Promise((resolve, reject) => {
      this.usersService.getUserByEmail(control.value).subscribe((user: User) => {
        if (user) {
          resolve({
            forbiddenEmail: true,
          });
        } else {
          resolve(null);
        }
      });

    });

  }

}

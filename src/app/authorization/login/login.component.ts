import {Component, EventEmitter, HostBinding, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../../shared/services/users.service';
import {User} from '../../shared/user/user.model';
import {Message} from '../../shared/message/message.model';
import {AuthService} from '../../shared/services/auth.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {fadeStateTrigger} from '../../shared/animations/fade.animations';
import {AnimationService} from '../../shared/services/animation.service';
import {fade1StateTrigger} from '../../shared/animations/fade1.animations';
import {TasksService} from '../../shared/services/tasks.service';
import {Task} from '../../shared/task/task.model';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [fadeStateTrigger, fade1StateTrigger],
})


export class LoginComponent implements OnInit, OnDestroy {
  clickCnt: string;
  user: User;
  task: Task;
  form: FormGroup;
  message: Message;
  closed = 'closed';
  open = 'open';
  fadeCanLogin = '';
  constructor(
    private animationService: AnimationService,
    private usersService: UsersService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private tasksService: TasksService
  ) {
    this.animationService.onClick.subscribe( cnt => this.clickCnt = cnt);
  }
  ngOnInit() {
   this.message = new Message('Danger', '');
    this.route.queryParams.subscribe((params: Params) => {
      if (params['nowCanLoggin']) {
        this.showMessage({
          text: 'Теперь вы можете зайти в систему',
          type: 'success'});
          this.fadeCanLogin = this.open;

      }
    }
    );
    this.form = new FormGroup( {
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null,  [Validators.required, Validators.minLength(6)])

    });
  }
  private showMessage(message: Message) {
    this.message = message;
    setTimeout(() => {
      this.fadeCanLogin = this.closed;
    }, 4000);
    window.setTimeout(() => {
     this.message.text = '';
    }, 5000);
  }

  toggle() {
    this.animationService.doClick(this.closed);
    setTimeout(() => {
      this.router.navigate(['/registration']);
      this.animationService.doClick(this.open);
    }, 500);
  }
  onSubmit() {
    const formData = this.form.value;

    this.tasksService.getTasks(1).subscribe(
      (task) => {
        window.localStorage.setItem('task', JSON.stringify(task));
      });

    this.usersService.getUserByEmail(formData.email).subscribe((user) => {
      if (user) {
        if (user.password === formData.password) {
          this.message.text = '';
          window.localStorage.setItem('user', JSON.stringify(user));
          this.authService.login();
          this.animationService.doClick(this.closed);
          setTimeout(() => {
            this.router.navigate(['system/planning']);
          }, 500 );
        } else {
          this.showMessage({
            text : 'Пароль не верный',
            type : 'danger'
          });
          setTimeout(() => {
          }, 3500 );
        }

      } else {
        this.showMessage({
          text: 'Такого пользователя не существует',
          type: 'danger'
        }); }
      this.user = user;
    });
  }
ngOnDestroy() {
  }
}

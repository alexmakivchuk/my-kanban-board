import {Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {fadeStateTrigger} from './shared/animations/fade.animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeStateTrigger]
})
export class AppComponent implements OnInit {
  b = 'closed';

ngOnInit() {
  setTimeout(() => {
    this.b = 'open';
  }, 100 );
}

}


